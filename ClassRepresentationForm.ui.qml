import QtQuick 2.4
import QtQuick.Window 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Rectangle {

    property string className
    property var classFunctions//: []
    property var classProperties//: []
    property string backGroundColor
    property var propertiesModelDataHeight: 15

    width: 100
    height: 150
    color: backGroundColor
    Drag.active: true
    Drag.hotSpot.x: (width - border.width * 2) / 2
    Drag.hotSpot.y: (height - border.width * 2) / 2

    border.color: "black"
    border.width: 2
    radius: 5

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 2
        spacing: 0

        Rectangle {
            height: 20
            Layout.preferredHeight: 20
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop

            Label {
                text: className
                anchors.centerIn: parent
            }
        }

        LineSeparator
        {
            Layout.preferredHeight: 1
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
        }

        Rectangle {
//            height: propertiesRepeater.model.length * propertiesModelDataHeight
//            Layout.minimumHeight: propertiesRepeater.model.length * propertiesModelDataHeight
            Layout.preferredHeight: propertiesRepeater.model.length * propertiesModelDataHeight
            Layout.fillWidth: true
//            Layout.fillHeight: true
            Layout.fillHeight: functionsRepeater.model.length === 0 ? true : false
            Layout.alignment: Qt.AlignTop

            Column {
                id: propertiesColumnLayout
                spacing: 1
                anchors.fill: parent
                Repeater {
                    id: propertiesRepeater
                    model: classProperties

                    Label
                    {
                        height: propertiesModelDataHeight
                        text: "- " + modelData
                        color: "#8ab597"
                    }
                }
            }

        }

        LineSeparator
        {
            Layout.preferredHeight: 1
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
        }

        Rectangle {
//            Layout.preferredHeight: 0
            Layout.preferredHeight: functionsRepeater.model.length * propertiesModelDataHeight
//            Layout.fillWidth: true
            Layout.fillHeight: functionsRepeater.model.length === 0 ? false : true
            Layout.alignment: Qt.AlignTop

            Column {
                id: functionsColumnLayout
                anchors.fill: parent
                spacing: 1
                Repeater {
                    id: functionsRepeater
                    model: classFunctions

                    Label
                    {
                        text: "+ " + modelData
    //                    font.pixelSize: 22
    //                    font.italic: true
                        color: "#cc7a7a"
                    }
                }
            }
        }


    }

}

