import QtQuick 2.4
import QtQuick.Controls 2.5

ClassRepresentationForm {

    id: classGUI
    className: "Class Name"

    classFunctions: []
    classProperties: []
    property int areaW:100 // background area size W init
    property int areaH:100 // background area size W init
    property var mainCanvas
    property var multyplicityLabels: []
    property var minWidth: 100
    property var minHeight: 150

    property bool somethingPressed : false

    MouseArea
    {
        id:areaResizableAnglesRightTop
        anchors.fill: parent
        anchors.margins: -5
        hoverEnabled: true
        cursorShape: !somethingPressed ? Qt.SizeBDiagCursor : Qt.ArrowCursor

        onPositionChanged:
        {
            if(!pressed)
                return

            if(mouseX > classGUI.width)
                classGUI.width = mouseX


            if(mouseX < classGUI.width && mouseX >= classGUI.minWidth)
                classGUI.width = mouseX

            if(mouseY < 0)
            {
                classGUI.y += mouseY

                classGUI.height -= mouseY
            }


            if(mouseY > 0 && (classGUI.height-mouseY) >= minHeight)
            {
                classGUI.y += mouseY

                classGUI.height -= mouseY
            }


            mainCanvas.requestPaint()

        }

        MouseArea
        {
            id:areaResizableAnglesLeftTop
            anchors.fill: parent
            anchors.rightMargin: 10
            anchors.bottomMargin: 10
            hoverEnabled: true
            cursorShape: !somethingPressed ? Qt.SizeFDiagCursor : Qt.ArrowCursor
            onPositionChanged:
            {
                if(!pressed)
                    return

                if(mouseX > 0 && (classGUI.width-mouseX) >= minWidth){

                    classGUI.x += mouseX
                    classGUI.width -= mouseX
                }


                if(mouseX < 0)
                {
                    classGUI.x += mouseX

                    classGUI.width -= mouseX
                }

                if(mouseY < 0)
                {
                    classGUI.y += mouseY

                    classGUI.height -= mouseY
                }


                if(mouseY > 0 && (classGUI.height-mouseY) >= minHeight)
                {
                    classGUI.y += mouseY

                    classGUI.height -= mouseY
                }

                mainCanvas.requestPaint()

            }
        }

        MouseArea
        {
            id:areaResizableAnglesRightBottom
            anchors.fill: parent
            anchors.leftMargin: 10
            anchors.topMargin: 10
            hoverEnabled: true
            cursorShape: !somethingPressed ? Qt.SizeFDiagCursor : Qt.ArrowCursor
            onPositionChanged:
            {
                if(!pressed)
                    return

                if(mouseX > classGUI.width)
                    classGUI.width = mouseX


                if(mouseX < classGUI.width && mouseX >= classGUI.minWidth)
                    classGUI.width = mouseX

                if(mouseY < classGUI.height && (classGUI.height-(classGUI.height - mouseY)) >= minHeight)
                {
                    classGUI.height -= classGUI.height - mouseY
                }


                if(mouseY > classGUI.height)
                {
                    classGUI.height -= classGUI.height - mouseY
                }

                mainCanvas.requestPaint()

            }
        }

        MouseArea
        {
            id:areaResizableAnglesLeftBottom
            anchors.fill: parent
            anchors.rightMargin: 10
            anchors.topMargin: 10
            hoverEnabled: true
            cursorShape: !somethingPressed ? Qt.SizeBDiagCursor : Qt.ArrowCursor
            onPositionChanged:
            {
                if(!pressed)
                    return

                if(mouseX > 0 && (classGUI.width-mouseX) >= minWidth){

                    classGUI.x += mouseX
                    classGUI.width -= mouseX
                }


                if(mouseX < 0)
                {
                    classGUI.x += mouseX

                    classGUI.width -= mouseX
                }

                if(mouseY < classGUI.height && (classGUI.height-(classGUI.height - mouseY)) >= minHeight)
                {
                    classGUI.height -= classGUI.height - mouseY
                }


                if(mouseY > classGUI.height)
                {
                    classGUI.height -= classGUI.height - mouseY
                }

                mainCanvas.requestPaint()

            }
        }

        MouseArea
        {
            id:areaResizableHorizontal
            anchors.fill: parent
            anchors.topMargin: 10
            anchors.bottomMargin: 10
            hoverEnabled: true
            cursorShape: !somethingPressed ? Qt.SizeHorCursor : Qt.ArrowCursor
            onPositionChanged:
            {
                if(!pressed)
                    return

                if(mouseX > classGUI.width / 2) //right
                {
                    if(mouseX > classGUI.width)
                        classGUI.width = mouseX


                    if(mouseX < classGUI.width && mouseX >= classGUI.minWidth)
                        classGUI.width = mouseX

                }
                else                            //left
                {
                    if(mouseX > 0 && (classGUI.width-mouseX) >= minWidth){

                        classGUI.x += mouseX
                        classGUI.width -= mouseX
                    }


                    if(mouseX < 0)
                    {
                        classGUI.x += mouseX

                        classGUI.width -= mouseX
                    }
                }

                mainCanvas.requestPaint()


            }
        }

        MouseArea
        {
            id:areaResizableVertical
            anchors.fill: parent
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            hoverEnabled: true
            cursorShape: !somethingPressed ? Qt.SizeVerCursor : Qt.ArrowCursor
            onPositionChanged:
            {
                if(!pressed)
                    return

                if(mouseY > classGUI.height / 2) //bottom
                {
                    if(mouseY < classGUI.height && (classGUI.height-(classGUI.height - mouseY)) >= minHeight)
                    {
                        classGUI.height -= classGUI.height - mouseY
                    }


                    if(mouseY > classGUI.height)
                    {
                        classGUI.height -= classGUI.height - mouseY
                    }

                }
                else                            //top
                {

                    if(mouseY < 0)
                    {
                        classGUI.y += mouseY

                        classGUI.height -= mouseY
                    }


                    if(mouseY > 0 && (classGUI.height-mouseY) >= minHeight)
                    {
                        classGUI.y += mouseY

                        classGUI.height -= mouseY
                    }
                }

                mainCanvas.requestPaint()


            }
        }

        MouseArea
        {
            id:area
            anchors.fill: parent
            anchors.margins: 10
            drag.target: parent.parent
            drag.maximumX: areaW - width
            drag.minimumX: 0
            drag.maximumY: areaH - height
            drag.minimumY: 0
            hoverEnabled: true
            clip: true
            cursorShape: pressed && !somethingPressed ? Qt.ClosedHandCursor : Qt.ArrowCursor
            property var counter : 0
            onPositionChanged:
            {
                counter++;
                if(!area.pressed /*|| counter % 10 != 0*/)
                    return
                mainCanvas.requestPaint()
            }
            onClicked: console.log("area")
        }
    }

    function getCenterV()
    {

    }

    function getCenterH()
    {

    }

    function setLabel(x, y, text)
    {

    }

    function removeMultyplicityLabels()
    {
        for(var i = 0 ; i < multyplicityLabels.length; i ++)
        {
            multyplicityLabels[i].destroy()
        }
        multyplicityLabels = []
    }

    function setMultyplicityLabel(text, currCenter, assotiatedCenter)
    {
        var osY = Math.abs(assotiatedCenter.y - currCenter.y)
        var osX = Math.abs(assotiatedCenter.x - currCenter.x)

        var newObject = Qt.createQmlObject('import QtQuick.Controls 2.5
    Label
    {
        id: multyplicityLabel
        text: "<blank>"


        function updatePos()
        {
            anchors.margins = 0
            x = 0
            y = 0
        }
    }',
                                           classGUI,
                                           currCenter.x + "_" + assotiatedCenter.x + "_" + assotiatedCenter.y + "_" + assotiatedCenter.y)

        multyplicityLabels.push(newObject)
        newObject.text = text
        if(currCenter.y > assotiatedCenter.y) // top
        {

            if(currCenter.x < assotiatedCenter.x) // right
            {
                if(osY / osX > classGUI.height / classGUI.width) ///top right angle : top
                {
                    newObject.updatePos()

                    newObject.anchors.bottom = classGUI.top
                    newObject.x = (classGUI.width / 2) + (classGUI.width / 2) * (osX / osY)
                }
                else ///top right angle : right
                {
                    newObject.updatePos()

                    newObject.anchors.left = classGUI.right
                    newObject.y =(classGUI.height / 2) - (osY / osX * (classGUI.width / 2))
                }
            }
            else // left
            {

                if(osY / osX > classGUI.height / classGUI.width) ///top left angle : top
                {
                    newObject.updatePos()

                    newObject.anchors.bottom = classGUI.top
                    newObject.x = (classGUI.width / 2) - (classGUI.height / 2) * osX / osY
                }
                else ///top left angle : left
                {

                    newObject.updatePos()

                    newObject.anchors.right = classGUI.left
                    newObject.y = classGUI.height / 2 - (classGUI.height / 2) * osY / osX

                    if((newObject.y + newObject.height) < 0)
                    {
                        newObject.y = - newObject.height
                    }
                }
            }
        }
        else // bottom
        {

            if(currCenter.x < assotiatedCenter.x) // right
            {
                if(osY / osX > classGUI.height / classGUI.width) ///bottom right angle : bottom
                {
                    newObject.updatePos()

                    newObject.anchors.top = classGUI.bottom
                    newObject.x = classGUI.width / 2 + osX * (classGUI.height / 2) / osY
                }
                else ///top right angle : right
                {
                    newObject.updatePos()

                    newObject.anchors.left = classGUI.right
                    newObject.y = classGUI.height / 2 + osY * (classGUI.width / 2) / osX
                }
            }
            else // left
            {

                if(osY / osX > classGUI.height / classGUI.width) ///bottom left angle : bottom
                {
                    newObject.updatePos()

                    newObject.anchors.top = classGUI.bottom
                    newObject.x = classGUI.width / 2 - osX * (classGUI.height / 2) / osY

                }
                else ///top left angle : right
                {
                    newObject.updatePos()

                    newObject.anchors.right = classGUI.left
                    newObject.y = classGUI.height / 2 + osY * (classGUI.width / 2) / osX
                }
            }
        }

    }

    function getEndOfArrowPos(currCenter, assotiatedCenter)
    {
        var osY = Math.abs(assotiatedCenter.y - currCenter.y)
        var osX = Math.abs(assotiatedCenter.x - currCenter.x)

        var result = Qt.point(0,0)
        if(currCenter.y > assotiatedCenter.y) // top
        {

            if(currCenter.x < assotiatedCenter.x) // right
            {
                if(osY / osX > classGUI.height / classGUI.width) ///top right angle : top
                {
                    result.y = currCenter.y - (classGUI.height / 2) + 10
                    result.x = currCenter.x + (classGUI.width / 2) * (osX / osY)
                }
                else ///top right angle : right
                {
                    result.x = currCenter.x + (classGUI.width / 2)
                    result.y =currCenter.y - (osY / osX * (classGUI.width / 2))
                }
            }
            else // left
            {

                if(osY / osX > classGUI.height / classGUI.width) ///top left angle : top
                {
                    result.y = currCenter.y - (classGUI.height / 2) + 10
                    result.x = currCenter.x - (classGUI.height / 2) * osX / osY
                }
                else ///top left angle : left
                {
                    console.log("top")
                    result.x = currCenter.x - (classGUI.width / 2) + 10
                    result.y = currCenter.y - (classGUI.height / 2) * osY / osX

                    if((result.y) < currCenter.y - (classGUI.height / 2))
                    {
                        result.y = currCenter.y - (classGUI.height / 2)
                    }
                }
            }
        }
        else // bottom
        {

            if(currCenter.x < assotiatedCenter.x) // right
            {
                if(osY / osX > classGUI.height / classGUI.width) ///bottom right angle : bottom
                {
                    result.y = currCenter.y + (classGUI.height / 2) + 10
                    result.x = currCenter.x + osX * (classGUI.height / 2) / osY
                }
                else ///bottom right angle : right
                {
                    result.x = currCenter.x + (classGUI.width / 2) + 10
                    result.y = currCenter.y + osY * (classGUI.width / 2) / osX
                }
            }
            else // left
            {

                if(osY / osX > classGUI.height / classGUI.width) ///bottom left angle : bottom
                {
                    result.y = currCenter.y + (classGUI.height / 2) + 10
                    result.x = currCenter.x - osX * (classGUI.height / 2) / osY

                }
                else ///bottom left angle : left
                {
                    console.log("bottom")
                    result.x = currCenter.x - (classGUI.width / 2) + 10
                    result.y = currCenter.y + osY * (classGUI.width / 2) / osX
                }
            }
        }

        return result
    }

    Component.onCompleted:
    {
//        setMultyplicityLabel("Suka", Qt.point(20, 20), Qt.point(10, 10))
    }
}


