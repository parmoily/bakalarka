import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: window
    width: 1280
    height: 720
    visible: true
    title: qsTr("Neo4J Visualiser")

    property string mainWindowBackgroundColor: "#bdbdbd"
    property string objectsBackgroundColor: "#ffffff"

    Rectangle
    {
        color: mainWindowBackgroundColor
        anchors.fill: parent

        ColumnLayout
        {
            anchors.fill: parent
            Item {
                id: header
                Layout.preferredHeight: 100
                Layout.fillWidth: true

                Rectangle {
                    color: objectsBackgroundColor
                    border.color: "grey"
                    border.width: 2
                    radius: 5
                    anchors.left: parent.left
                    anchors.right: rightBtn.left
                    anchors.rightMargin: 10
                    anchors.leftMargin: 10
                    height: 50
                    anchors.verticalCenter: parent.verticalCenter
                    MouseArea
                    {
                        anchors.fill: parent
                        cursorShape: Qt.IBeamCursor
                        propagateComposedEvents : true
                        hoverEnabled : true

                        TextInput {
                            id: input
                            text: "Type your request"
                            color: "#151515"; selectionColor: "green"
                            font.pixelSize: 16; font.bold: true
                            width: parent.width - 20
                            anchors.centerIn: parent
                            focus: true
                            readOnly:false
                            selectByMouse: true
                        }
                    }
                }

                Button {
                    id: rightBtn
                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    anchors.verticalCenter: parent.verticalCenter
                    height: 50
                    width: 50
                    text: "Run"
                    onClicked: {
                        console.log(assotiationModel.getAssociations())
                    }
                }
            }

            Rectangle {
                id: diagrams
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.margins: 10
                border.color: "grey"
                border.width: 2
                radius: 5

                color: objectsBackgroundColor


                Canvas
                {
                    anchors.fill: parent
                    id:canvas

                    property int assotiationsCount: 0
                    property var assotiationsFIDs: []
                    property var assotiationsSIDs: []
                    property var assotiationsNames: []
                    property var assotiationsDirection: []
                    property var firstMultiplicities: []
                    property var secondMultiplicities: []

                    signal assotiationsChanged()
                    Component.onCompleted: assotiationModel.assotiationsChanged.connect(assotiationsChanged)
                    onAssotiationsChanged: {
                            print("kaka")
                            updateAssotiations()
                        }

//                    Connections{
//                        target: assotiationModel
//                        function onAssotiationsChanged() {
//                            updateAssotiations()
//                            print("kaka")
//                        }
//                    }

                    function getInvertedDirection(direct)
                    {
                        if(direct === "")
                            return direct

                        if(direct === "▶")
                        {
                            return "◀"
                        }
                        else
                        {
                            return "▶"
                        }

                    }
                    function arrow(context, fromx, fromy, tox, toy) {
                         const dx = tox - fromx;
                         const dy = toy - fromy;
                         const headlen = Math.sqrt(dx * dx + dy * dy) * 0.3; // length of head in pixels
                         const angle = Math.atan2(dy, dx);
                         context.beginPath();
                         context.moveTo(fromx, fromy);
                         context.lineTo(tox, toy);
                         context.stroke();
                         context.beginPath();
                         context.moveTo(tox - headlen * Math.cos(angle - Math.PI / 6), toy - headlen * Math.sin(angle - Math.PI / 6));
                         context.lineTo(tox, toy );
                         context.lineTo(tox - headlen * Math.cos(angle + Math.PI / 6), toy - headlen * Math.sin(angle + Math.PI / 6));
                         context.stroke();
                       }

                    function updateAssotiations()
                    {
                        print("kaka")
                        assotiationsFIDs = []
                        assotiationsSIDs = []
                        assotiationsNames = []
                        assotiationsDirection = []
                        firstMultiplicities = []
                        secondMultiplicities = []

                        assotiationsCount = assotiationModel.associationsCount()
                        for(var i = 0 ; i < assotiationsCount; i ++)
                        {
//                            var first = Qt.point(classesGridLayout.getCenterH(assotiationModel.getAssociation(i).firsClassID()),
//                                                 classesGridLayout.getCenterV(assotiationModel.getAssociation(i).firsClassID()))
//                            var second = Qt.point(classesGridLayout.getCenterH(assotiationModel.getAssociation(i).secondClassID()),
//                                                  classesGridLayout.getCenterV(assotiationModel.getAssociation(i).secondClassID()))
                            var first = assotiationModel.getAssociation(i).firsClassID()
                            var second = assotiationModel.getAssociation(i).secondClassID()

                            assotiationsFIDs.push(first)
                            assotiationsSIDs.push(second)
                            assotiationsNames.push(assotiationModel.getAssociation(i).associationName())
                            assotiationsDirection.push(assotiationModel.getAssociation(i).direction())
                            firstMultiplicities.push(assotiationModel.getAssociation(i).firstMultiplicity())
                            secondMultiplicities.push(assotiationModel.getAssociation(i).secondMultiplicity())
                        }
                    }

                    onPaint:{
                       var ctx = canvas.getContext('2d');
                       //...

                        // Make canvas all white
                        ctx.beginPath();
                        ctx.clearRect(0, 0, width, height);
                        ctx.fill();

                        ctx.strokeStyle = Qt.rgba(0, 0, 0, 1);
                        ctx.lineWidth = 1;

                        classesGridLayout.deleteMultyplicities()
                        classesGridLayout.deleteAssotiationsLabels()

                        for(var i = 0 ; i < assotiationsCount; i ++)
                        {
                            ctx.beginPath();

                            var first = Qt.point(classesGridLayout.getCenterH(assotiationsFIDs[i]),
                                        classesGridLayout.getCenterV(assotiationsFIDs[i]))

                            var second = Qt.point(classesGridLayout.getCenterH(assotiationsSIDs[i]),
                                                  classesGridLayout.getCenterV(assotiationsSIDs[i]))


                            var lineCenter = Qt.point(
                                        Math.min(first.x, second.x) + (Math.abs(first.x - second.x))/2,
                                        Math.min(first.y, second.y) + (Math.abs(first.y - second.y))/2
                                        )


                            var firstRowEnd = classesGridLayout.getEndOfArrowPos(assotiationsFIDs[i], assotiationsSIDs[i])
                            var secondRowEnd = classesGridLayout.getEndOfArrowPos(assotiationsSIDs[i], assotiationsFIDs[i])
//                            ctx.moveTo(firstRowEnd.x, firstRowEnd.y);//start point
//                            ctx.lineTo(secondRowEnd.x, secondRowEnd.y);//end point

//                            ctx.stroke();
                            arrow(ctx, firstRowEnd.x, firstRowEnd.y, secondRowEnd.x, secondRowEnd.y)

                            ctx.font.antialiasing = false
                            ctx.fillStyle  = "black";
                            ctx.font = "15px Arial";
//                            ctx.fillText(assotiationsNames[i], lineCenter.x, lineCenter.y)


                            var currentDirection

                            if(first.x < second.x)
                            {
                                currentDirection = assotiationsDirection[i]
                            }
                            else
                            {
                                currentDirection = getInvertedDirection(assotiationsDirection[i])
                            }

                            ctx.font = "23px Arial";
                            classesGridLayout.setAssotiationsLabel(assotiationsNames[i], currentDirection,
                                                                   assotiationsFIDs[i], assotiationsSIDs[i])
//                            ctx.fillText(currentDirection, lineCenter.x + assotiationsNames[i].length * 8, lineCenter.y + 4)

                            ctx.font = "15px Arial";


                            classesGridLayout.setMultyplicityLabel(firstMultiplicities[i], assotiationsFIDs[i], assotiationsSIDs[i])
                            classesGridLayout.setMultyplicityLabel(secondMultiplicities[i], assotiationsSIDs[i], assotiationsFIDs[i])
//                            ctx.fillText(firstMultiplicities[i], first.x + fistMultiplicityW, first.y + fistMultiplicityH)
//                            ctx.fillText(secondMultiplicities[i], second.x + secondMultiplicityW, second.y + secondMultiplicityH)


                        }


                    }
                }

                GridLayout
                {
                    id: classesGridLayout
                    anchors.fill: diagrams
                    anchors.margins: 10
                    property var assotiationsLabels: []

                    Repeater {
                        id: classesRepeater
                        model: mainWindow.classesModel
                        ClassRepresentation
                        {
                            className: model.className
                            classProperties: model.propertiesList
                            classFunctions: model.functionsList
                            backGroundColor: objectsBackgroundColor
                            areaW: parent.width
                            areaH: parent.height
                            mainCanvas: canvas


                            Component.onCompleted:
                            {
                                console.log(model.propertiesList)
                            }
                        }
                    }

                    ///Get vertical center position of object it repeater with index="index"
                    function getCenterV(index)
                    {
                        return classesRepeater.itemAt(index).y + classesRepeater.itemAt(index).height / 2
                    }

                    ///Get horizontal center position of object it repeater with index="index"
                    function getCenterH(index)
                    {
                        return classesRepeater.itemAt(index).x + classesRepeater.itemAt(index).width / 2
                    }

                    function getCenterWidth(index)
                    {
                        return classesRepeater.itemAt(index).width
                    }

                    function getCenterHeight(index)
                    {
                        return classesRepeater.itemAt(index).height
                    }

                    function setMultyplicityLabel(text, index1, index2)
                    {

                        var firstPoint = Qt.point(getCenterH(index1), getCenterV(index1))
                        var secondPoint = Qt.point(getCenterH(index2), getCenterV(index2))
                        classesRepeater.itemAt(index1).setMultyplicityLabel(text, firstPoint, secondPoint)
                    }

                    function getEndOfArrowPos(index1, index2)
                    {

                        var firstPoint = Qt.point(getCenterH(index1), getCenterV(index1))
                        var secondPoint = Qt.point(getCenterH(index2), getCenterV(index2))
                        return classesRepeater.itemAt(index1).getEndOfArrowPos(firstPoint, secondPoint)
                    }

                    function deleteMultyplicities()
                    {
                        for(var i = 0 ; i < classesRepeater.count; i ++)
                        {
                            classesRepeater.itemAt(i).removeMultyplicityLabels()
                        }
                    }

                    function deleteAssotiationsLabels()
                    {
                        for(var i = 0 ; i < assotiationsLabels.length; i ++)
                        {
                            assotiationsLabels[i].destroy()
                        }
                        assotiationsLabels = []
                    }

                    function setAssotiationsLabel(text, directionText, firstIndx, secondIndx)
                    {
                        var newObject = Qt.createQmlObject(
                        '
import QtQuick 2.4
import QtQuick.Controls 2.5
                            Label
                            {
                                text: "<blank>"

                            MouseArea
                            {
                                id:area
                                anchors.fill: parent
                                drag.target: parent
                                drag.maximumX: parent.parent.width - width
                                drag.minimumX: 0
                                drag.maximumY: parent.parent.height - height
                                drag.minimumY: 0
                                hoverEnabled: true
                                clip: true
                                cursorShape: Qt.ClosedHandCursor
                                onPositionChanged:
                                {
                                }
                            }

                                Component.onCompleted:
                                {
//                                    console.log(area.width)
                                }
                                function updatePos()
                                {
                                    anchors.margins = 0
                                    x = 0
                                    y = 0
                                }
                            }
                        ',
                       canvas,
                       firstIndx + "_" + secondIndx)

                        assotiationsLabels.push(newObject)

                       newObject.updatePos()

                        newObject.text = text + " " + directionText

                        var leftObj, rightObj, topObj, bottomObj

                        leftObj = getCenterH(firstIndx) < getCenterH(secondIndx) ?
                                    classesRepeater.itemAt(firstIndx) : classesRepeater.itemAt(secondIndx)

                        rightObj = getCenterH(firstIndx) > getCenterH(secondIndx) ?
                                    classesRepeater.itemAt(firstIndx) : classesRepeater.itemAt(secondIndx)

                        topObj = getCenterV(firstIndx) < getCenterV(secondIndx) ?
                                    classesRepeater.itemAt(firstIndx) : classesRepeater.itemAt(secondIndx)

                        bottomObj = getCenterV(firstIndx) > getCenterV(secondIndx) ?
                                    classesRepeater.itemAt(firstIndx) : classesRepeater.itemAt(secondIndx)

                        newObject.x = (leftObj.x + leftObj.width)+ /*Math.abs*/(rightObj.x - (leftObj.x + leftObj.width)) / 2

//                        console.log(text, topObj.y, bottomObj.y, topObj.height, bottomObj.height)
                        newObject.y = (topObj.y + topObj.height) + /*Math.abs*/(bottomObj.y - (topObj.y + topObj.height)) / 2

                    }


                }
            }


        }

    }


}

