# This Python file uses the following encoding: utf-8
import os
import sys
import random
from MainWindow import MainWindow
from PySide2.QtWidgets import (QApplication, QLabel, QPushButton,
                               QVBoxLayout, QWidget)
from PySide2.QtCore import Slot, Qt
from PySide2.QtCore import QObject, Signal, Property, QUrl
import PySide2.QtQml
from PySide2 import QtCore
from PySide2.QtQuick import QQuickView
from PySide2.QtCore import QStringListModel, Qt, QUrl
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine
from neo4j import GraphDatabase
from ClassRepresentation import ClassRepresentationModel
from ClassRepresentation import ClassRepresentation
from AssociationRepresentation import AssociationRepresentationModel
from AssociationRepresentation import AssociationRepresentation
from AssociationRepresentation import Multiplicity
from AssociationRepresentation import Direction

class MyWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.hello = ["Hallo Welt", "你好，世界", "Hei maailma",
            "Hola Mundo", "Привет мир"]

        self.button = QPushButton("Click me!")
        self.text = QLabel("Hello World")
        self.text.setAlignment(Qt.AlignCenter)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)

        # Connecting the signal
        self.button.clicked.connect(self.magic)

    @Slot()
    def magic(self):
        self.text.setText(random.choice(self.hello))



class HelloWorldExample:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def print_greeting(self, message):
        with self.driver.session() as session:
            greeting = session.write_transaction(self._create_and_return_greeting, message)
            print(greeting)

    @staticmethod
    def _create_and_return_greeting(tx, message):
        result = tx.run("CREATE (a:Greeting) "
                        "SET a.message = $message "
                        "RETURN a.message + ', from node ' + id(a)", message=message)
        return result.single()[0]

class Neo4jConnection(QtCore.QObject):

    def __init__(self, uri, user, pwd, parent=None):
        QObject.__init__(self, parent)
        self.__uri = uri
        self.__user = user
        self.__pwd = pwd
        self.__driver = None
        try:
            self.__driver = GraphDatabase.driver(self.__uri, auth=(self.__user, self.__pwd))
        except Exception as e:
            print("Failed to create the driver:", e)

    def close(self):
        if self.__driver is not None:
            self.__driver.close()


    @QtCore.Slot(str, result=str)
    def query(self, query, db=None):
        assert self.__driver is not None, "Driver not initialized!"
        session = None
        response = None
        try:
            session = self.__driver.session(database=db) if db is not None else self.__driver.session()
            response = list(session.run(query))
            print(response)
        except Exception as e:
            print("Query failed:", e)
        finally:
            if session is not None:
                session.close()
        print(type(response[0].data()))
        print(response[0].data())
#        print(" ".join(response))
#        stringVal = " ".join(response)
        return response[0]

if __name__ == "__main__":
    app = QGuiApplication(sys.argv)

    view = QQmlApplicationEngine()

    greeter = Neo4jConnection("bolt://localhost:7687", "neo4j", "1234")
#    view.rootContext().setContextProperty("neo4j",greeter)

    context = view.rootContext()
    mainWindow = MainWindow()

    # class1 =  ClassRepresentation("Ilya", ["krasivyj : bool", "seksualen : bool"], ["pokushat() :int"])
    # classesModel = ClassRepresentationModel([class1])
    # assotiationModel = AssociationRepresentationModel([])

    class1 =  ClassRepresentation("Ilya", ["krasivyj : bool", "seksualen : bool"], ["pokushat() :int"])
    class2 =  ClassRepresentation("Juliasha", ["Krasivaja : bool", "ilyushina: bool"], ["pokushat() :int"])
    class3 =  ClassRepresentation("Koshka", ["glupaja :String"], ["pokushat() :int", "myau: bool"])
    class4 =  ClassRepresentation("Korm", ["est :int"], ["getEst() :int"])
    association = AssociationRepresentation(class1, class2, "Ljubov", Multiplicity.ONE, Multiplicity.ONE, Direction.FIRST_TO_SECOND)
    association2 = AssociationRepresentation(class1, class3, "Ljubov", Multiplicity.ONE, Multiplicity.ONE, Direction.FIRST_TO_SECOND)
    association3 = AssociationRepresentation(class2, class3, "Ljubov", Multiplicity.ONE, Multiplicity.ONE, Direction.FIRST_TO_SECOND)
    association4 = AssociationRepresentation(class3, class4, "Zhryot", Multiplicity.ONE, Multiplicity.ONE_TO_MANY, Direction.FIRST_TO_SECOND)
    classesModel = ClassRepresentationModel([class1, class2, class3, class4])
    assotiationModel = AssociationRepresentationModel([association, association2, association3, association4])
    mainWindow.setClassesModel(classesModel)


    context.setContextProperty("mainWindow", mainWindow)
    context.setContextProperty("assotiationModel", assotiationModel)

    view.load(QUrl('main.qml'))
    qml_file = os.path.join(os.path.dirname(__file__),"main.qml")

    assotiationModel.updQML()
#    greeter.query("CREATE (a:Person {name:'Ilya'})")

    #execute and cleanup
#    app.exec_()
#    del view
#    greeter.close()

    rc = app.exec_()
    sys.exit(rc)
