# This Python file uses the following encoding: utf-8
from PySide2 import QtCore
from enum import Enum
from PySide2.QtCore import QAbstractListModel, QModelIndex, Qt

class Direction(Enum):
	NONE = 1
	FIRST_TO_SECOND = 2
	SECOND_TO_FIRST = 3

class Multiplicity(Enum):
	NONE = 1
	ONE = 2
	NULL_TO_MANY = 3
	ONE_TO_MANY = 4

def directionSwitch(i):
    switcher={
            Direction.NONE:"",
            Direction.FIRST_TO_SECOND:"▶",
            Direction.SECOND_TO_FIRST:"◀"
         }
    return switcher.get(i,"Invalid Direction")

def multiplicitySwitch(i):
    switcher={
            Multiplicity.NONE:"",
            Multiplicity.ONE:"1",
            Multiplicity.NULL_TO_MANY:"0..*",
            Multiplicity.ONE_TO_MANY:"1..*"
         }
    return switcher.get(i,"Invalid Multiplicity")


class AssociationRepresentation(QtCore.QObject):
    def __init__(self, firsClass, secondClass, associationName = "", firstMultiplicity = Multiplicity(1), secondMultiplicity = Multiplicity(1), direction = Direction(1)):
        QtCore.QObject.__init__(self, None)
        self._firsClass = firsClass
        self._secondClass = secondClass
        self._associationName = associationName
        self._firstMultiplicity = firstMultiplicity
        self._secondMultiplicity = secondMultiplicity
        self._direction = direction

    @QtCore.Slot(result=int)
    def firsClassID(self) -> int:
        return self._firsClass.getIdGUI()
    @QtCore.Slot(result=int)
    def secondClassID(self) -> int:
        return self._secondClass.getIdGUI()
    @QtCore.Slot(result=str)
    def associationName(self) -> str:
        return self._associationName
    @QtCore.Slot(result=str)
    def firstMultiplicity(self) -> str:
        return multiplicitySwitch(self._firstMultiplicity)
    @QtCore.Slot(result=str)
    def secondMultiplicity(self) -> str:
        return multiplicitySwitch(self._secondMultiplicity)
    @QtCore.Slot(result=str)
    def direction(self) -> str:
        return directionSwitch(self._direction)

class AssociationRepresentationModel(QAbstractListModel):
    assotiationsChanged = QtCore.Signal(name='assotiationsChanged')

    AssociationName = Qt.UserRole + 1
    FirstMultiplicity = Qt.UserRole + 2
    SecondMultiplicity = Qt.UserRole + 3
    Direction = Qt.UserRole + 4
    FirstClassID = Qt.UserRole + 5
    SecondClassID = Qt.UserRole + 6

    _roles = {AssociationName: b"associationName", FirstMultiplicity: b"firstMultiplicity", SecondMultiplicity: b"secondMultiplicity", 
    Direction: b"direction", FirstClassID: b"firstClassID", SecondClassID: b"secondClassID",}

    def __init__(self, dataList=None, parent=None):
        QAbstractListModel.__init__(self, parent)

        self._assotiations = dataList

    def addData(self, data):
        self.beginInsertRows(QModelIndex(), self.rowCount(), self.rowCount())
        self._assotiations.append(data)
        self.endInsertRows()


    @QtCore.Slot(int, result=QtCore.QObject)
    def getAssociation(self, i):
        return self._assotiations[i]
    @QtCore.Slot(result=QtCore.QObject)
    def getAssociations(self):
        return self._assotiations

    def rowCount(self, parent=QModelIndex()):
        return len(self._assotiations)

    @QtCore.Slot(result=int)
    def associationsCount(self) -> int:
        return len(self._assotiations)

    def data(self, index, role=Qt.DisplayRole):
        try:
            data = self._assotiations[index.row()]
        except IndexError:
            return QVariant()

        if role == self.AssociationName:
            return data.className()

        if role == self.FirstMultiplicity:
            return data.attributesList()

        if role == self.SecondMultiplicity:
            return data.attributesList()

        if role == self.Direction:
            return data.attributesList()

        if role == self.FirstClassID:
            return data.attributesList()

        if role == self.SecondClassID:
            return data.attributesList()

        return QVariant()

    def roleNames(self):
        return self._roles

    def updQML(self):
        print("updQML")
        self.assotiationsChanged.emit()
