# This Python file uses the following encoding: utf-8
from PySide2 import QtCore
from PySide2.QtCore import QAbstractListModel, QModelIndex, Qt

class ClassRepresentation(QtCore.QObject):
    def __init__(self):
        pass

class ClassRepresentation(QtCore.QObject):
    def __init__(self, className="Default Class Name", propertiesList = [], functionsList = []):
        QtCore.QObject.__init__(self, None)
        self._className = className
        self._propertiesList = propertiesList
        self._functionsList = functionsList

    def className(self):
        return self._className
    def propertiesList(self):
        return self._propertiesList
    def functionsList(self):
        return self._functionsList
    def getGUIClassID(self):
        return 0
    def setParentModel(self, parentModel):
        self._parentModel = parentModel
    def getIdGUI(self):
        return self._parentModel.getGUIClassID(self)

class ClassRepresentationModel(QAbstractListModel):

    ClassName = Qt.UserRole + 1
    PropertiesList = Qt.UserRole + 2
    FunctionsList = Qt.UserRole + 3

    _roles = {ClassName: b"className", PropertiesList: b"propertiesList", FunctionsList: b"functionsList"}

    def __init__(self, dataList=None, parent=None):
        QAbstractListModel.__init__(self, parent)

        self._classes = dataList

        for x in self._classes:
            x.setParentModel(self)

    def addData(self, data):
        self.beginInsertRows(QModelIndex(), self.rowCount(), self.rowCount())
        self._classes.append(data)

        for x in data:
            x.setParentModel(self)

        self.endInsertRows()

    def rowCount(self, parent=QModelIndex()):
        return len(self._classes)

    def data(self, index, role=Qt.DisplayRole):
        try:
            data = self._classes[index.row()]
        except IndexError:
            return QVariant()

        if role == self.ClassName:
            return data.className()

        if role == self.PropertiesList:
            return data.propertiesList()

        if role == self.FunctionsList:
            return data.functionsList()

        return QVariant()

    def roleNames(self):
        return self._roles

    def getGUIClassID(self, classVal):
        currId = 0
        for x in self._classes:
            if x == classVal:
                return currId
            currId+=1
        return -1

